import os
import json
import boto3

from flask import Flask, jsonify, request
app = Flask(__name__)

USERS_TABLE = os.environ['USERS_TABLE']
BUCKET_NAME = os.environ['BUCKET_NAME']
IS_OFFLINE = os.environ.get('IS_OFFLINE')

if IS_OFFLINE:
    dyClient = boto3.client(
        'dynamodb',
        aws_access_key_id='',
        aws_secret_access_key='',
        region_name='localhost',
        endpoint_url='http://localhost:8000')
else:
    dyClient = boto3.client('dynamodb', region_name='us-east-1')

s3Client = boto3.client('s3', region_name='us-east-1')

@app.route("/")
def hello():
    if IS_OFFLINE:
        return 'Hello World * off-line *'
    else:
        return 'Hello World * on-line *'

@app.route("/users/<string:user_id>")
def get_user(user_id):
    resp = dyClient.get_item(
        TableName=USERS_TABLE,
        Key={
            'userId': { 'S': user_id }
        }
    )
    item = resp.get('Item')

    if not item:
        return jsonify({'error': 'User does not exist'}), 404

    return jsonify({
        'userId': item.get('userId').get('S'),
        'name': item.get('name').get('S')
    })

@app.route("/users", methods=["POST"])
def create_user():
    user_id = request.json.get('userId')
    name = request.json.get('name')

    if not user_id or not name:
        return jsonify({'error': 'Please provide userId and name'}), 400

    resp = dyClient.put_item(
        TableName=USERS_TABLE,
        Item={
            'userId': { 'S': user_id },
            'name': { 'S': name }
        }
    )

    return jsonify({
        'userId': user_id,
        'name': name
    })

@app.route("/users/<string:file_name>", methods=["POST"])
def create_file(file_name):
    data = json.dumps(request.json)

    resp = s3Client.put_object(Body=data, Bucket=BUCKET_NAME, Key=file_name)

    return json.dumps(request.json)
